//let skrollr = require('skrollr');

$(document).ready(function() {
    let search = $('.js-search');
    let close = $('.js-search-close');
    let menu = $('.b-header__hamburgers');
    let menuBg = $('.b-menu__bg');
    let closeAlert = $('.b-alert__close');

    search.on('click', function() {
        let $this = $(this);
        let parent = $this.closest('.b-header__content');
        let width = parent.find('.b-header__search')
        let menu = parent.find('.b-header__main-menu')
        let form = parent.find('.b-form')
        let logo = parent.find('.b-header__logo')

        width.addClass('move')
        close.addClass('active')
        logo.addClass('hidden')
        menu.addClass('hidden')
        setTimeout(function() {
            form.show()
        }, 300)

    });

    close.on('click', function() {
        let $this = $(this);
        let parent = $this.closest('.b-header__content');
        let width = parent.find('.b-header__search')
        let menu = parent.find('.b-header__main-menu')
        let form = parent.find('.b-form')
        let logo = parent.find('.b-header__logo')

        form.hide()
        setTimeout(function() {
            width.removeClass('move')
            close.removeClass('active')
        }, 200)
        setTimeout(function() {
            menu.removeClass('hidden')
            logo.removeClass('hidden')
        }, 300)
    });

    menu.on('click', function() {
        let $this = $(this);
        let body = $this.closest('body');
        let header = $this.closest('.b-header');
        let blockMenu = body.find('.b-menu')
        body.toggleClass('hidden')
        header.toggleClass('b-header--bg-black-js')
        blockMenu.toggleClass('active')
        $this.toggleClass('close')
    });

    menuBg.on('click', function() {
        let $this = $(this);
        let body = $this.closest('body');
        let header = $('.b-header');
        let blockMenu = body.find('.b-menu')
        body.removeClass('hidden')
        header.removeClass('b-header--bg-black-js')
        blockMenu.removeClass('active')
        menu.removeClass('close')
    });

    closeAlert.on('click', function() {
        let $this = $(this);
        $this.closest('.b-alert').addClass('b-alert--close')
        $this.closest('body').find('.b-wrapper').removeClass('b-wrapper--alert')
    });
});
