import objectFitImages from 'object-fit-images';

import './slider'
import './poll'
import './notify'
import './upload'
import './map'
import './form'
import './header'
import './aside'
import './mobile'
import './errorText'
import './tabs'
import './srolltop'
import './browser'
import './common'
import './popup'
import './scroll'
import './limitText'
import './like'

$(function() {
    objectFitImages()
});

