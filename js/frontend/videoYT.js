$(document).ready(function() {
    var player;
    if ($('#ytplayer').length) {
        player = new YT.Player('ytplayer', {
            events: {
                'onStateChange': function(event) {
                    if ((event.data == YT.PlayerState.PAUSED)) {
                        let parent = $('.b-poster__video-play').closest('.b-poster__video');
                        parent.removeClass('b-poster__video--hidden')
                        //parent.find('.b-poster__video-content').removeClass('b-poster__video-content--hidden')
                    }

                }
            }
        });
    }

    $('.b-poster__video-play').on('click', function() {
        let $this = $(this);
        let parent = $this.closest('.b-poster__video');
        parent.addClass('b-poster__video--hidden')
        //parent.find('.b-poster__video-content').addClass('b-poster__video-content--hidden')
        player.playVideo();
    })
})
