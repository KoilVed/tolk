$(document).ready(function () {
    let linkTab = $(".js-tab > div"),
        contentTab = $(".js-tab-content > div"),
        elClosest = '.b-lc__wrap';

    linkTab.on("click", function (e) {
        let $this = $(this),
            tabActive = $this.closest(elClosest).find('.js-tab');

        tabActive.find('div').removeClass("active");
        $this.addClass("active");
        let clickedTab = tabActive.find('.active');

        contentTab.removeClass("active");
        let clickedTabIndex = clickedTab.index();
        contentTab.eq(clickedTabIndex).addClass("active");
    });
});
