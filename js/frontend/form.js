$(document).ready(function() {
    var formAnimatedInput = $('.b-form__item > input');

    formAnimatedInput.on('focus', function () {
        $(this).addClass('is-filled');
    });

    formAnimatedInput.on('blur', function () {
        if ($(this).val().length == 0) {
            $(this).removeClass('is-filled');
        }
    });

    $('#avatarUpload').change(function(){
        function readImgUrlAndPreview(input){
            let reader = new FileReader();
            if (input.files && input.files[0]) {
                reader.onload = function (e) {
                    $('#avatarPreview').attr('src', e.target.result);
                }
            };
            reader.readAsDataURL(input.files[0]);
        }
        readImgUrlAndPreview(this);
    });

    // $('#secondPass').keyup(function(e){
    //     if($('#firstPass').val() !== $('#secondPass').val()){
    //         $('#error').removeClass('hidden');
    //         return false;
    //     }else{
    //         $('#error').addClass('hidden');
    //     }
    // });

    // var formAnimatedarea = $('.b-form__item > textarea');
    //
    // formAnimatedarea.on('focus', function () {
    //     $(this).addClass('is-filled');
    // });
    //
    // formAnimatedarea.on('blur', function () {
    //     if ($(this).val().length == 0) {
    //         $(this).removeClass('is-filled');
    //     }
    // });
});
