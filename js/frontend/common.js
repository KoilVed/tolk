$(document).ready(function () {
   let hashTag = $('.js-show-hash-tag');
   hashTag.on('click', function () {
       let $this = $(this);
       let hiddenTag = $this.parent().children();
       $this.addClass('active');
       hiddenTag.removeClass('hidden');
   })

    let linkYouTube = $('.js-youtube');

    linkYouTube.on('click', function (e) {
        e.stopPropagation();
        let url = $(this).attr('data-link');
        document.location.href = url;
    });

    $(document).on('click', '.filter', function() {
        let $this = $(this);
        let dropdown = $this.find('.filter__dropdown');
        dropdown.toggleClass('active')
    });
});
