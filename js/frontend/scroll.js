$(document).ready(function() {
    var a = document.querySelector('.js-fixed-sroll'), b = null, P = 30;  // если ноль заменить на число, то блок будет прилипать до того, как верхний край окна браузера дойдёт до верхнего края элемента. Может быть отрицательным числом

    var mq = window.matchMedia('(min-width: 768px)');
    if (mq.matches) {
        window.addEventListener('scroll', Ascroll, false);
        document.body.addEventListener('scroll', Ascroll, false);
    }

    function Ascroll() {
        if (a == null) {
            return false;
        }
        if (b == null) {
            let Sa = getComputedStyle(a, ''), s = '';
            for (var i = 0; i < Sa.length; i++) {
                if (Sa[i].indexOf('overflow') == 0 || Sa[i].indexOf('padding') == 0 || Sa[i].indexOf('border') == 0 || Sa[i].indexOf('outline') == 0 || Sa[i].indexOf('box-shadow') == 0 || Sa[i].indexOf('background') == 0) {
                    s += Sa[i] + ': ' + Sa.getPropertyValue(Sa[i]) + '; '
                }
            }
            b = document.createElement('div');
            b.style.cssText = s + ' box-sizing: border-box; width: ' + a.offsetWidth + 'px;';
            a.insertBefore(b, a.firstChild);
            var l = a.childNodes.length;
            for (var i = 1; i < l; i++) {
                b.appendChild(a.childNodes[1]);
            }
            a.style.height = b.getBoundingClientRect().height + 'px';
            a.style.padding = '0';
            a.style.border = '0';
            a.style.zIndex = '100';
        }

        let stopBlock = document.querySelector('.js-stop-aside-scroll');
        let bottomBorder = stopBlock.getBoundingClientRect().top;
        let Ra = a.getBoundingClientRect();
        let  R = Math.round(Ra.top + b.getBoundingClientRect().height - bottomBorder + 80);  // селектор блока, при достижении верхнего края которого нужно открепить прилипающий элемент;  Math.round() только для IE; если ноль заменить на число, то блок будет прилипать до того, как нижний край элемента дойдёт до футера
        let topMinusConst = Ra.top - P;

        if (topMinusConst <= 0) {

            if (topMinusConst <= R) {
                b.className = 'block-stop';
                b.style.top = -R + 'px';
            } else {
                b.className = 'block-fix';
                b.style.top = P + 'px';
            }
        } else {
            b.className = '';
            b.style.top = '';
        }
        window.addEventListener('resize', function() {
            a.children[0].style.width = getComputedStyle(a, '').width
        }, false);
    }

    var a2 = document.querySelector('.js-fixed-sroll-2'), b2 = null, P2 = 30;  // если ноль заменить на число, то блок будет прилипать до того, как верхний край окна браузера дойдёт до верхнего края элемента. Может быть отрицательным числом

    var mq2 = window.matchMedia('(min-width: 768px)');
    if (mq2.matches) {
        window.addEventListener('scroll', Ascroll2, false);
        document.body.addEventListener('scroll', Ascroll2, false);
    }

    function Ascroll2() {
        if (a2 == null) {
            return false;
        }
        if (b2 == null) {
            let Sa = getComputedStyle(a2, ''), s = '';
            for (var i = 0; i < Sa.length; i++) {
                if (Sa[i].indexOf('overflow') == 0 || Sa[i].indexOf('padding') == 0 || Sa[i].indexOf('border') == 0 || Sa[i].indexOf('outline') == 0 || Sa[i].indexOf('box-shadow') == 0 || Sa[i].indexOf('background') == 0) {
                    s += Sa[i] + ': ' + Sa.getPropertyValue(Sa[i]) + '; '
                }
            }
            b2 = document.createElement('div');
            b2.style.cssText = s + ' box-sizing: border-box; width: ' + a2.offsetWidth + 'px;';
            a2.insertBefore(b2, a2.firstChild);
            var l = a2.childNodes.length;
            for (var i = 1; i < l; i++) {
                b2.appendChild(a2.childNodes[1]);
            }
            a2.style.height = b2.getBoundingClientRect().height + 'px';
            a2.style.padding = '0';
            a2.style.border = '0';
            a2.style.zIndex = '100';
        }

        let stopBlock = document.querySelector('.js-stop-aside-scroll-2');
        let bottomBorder = stopBlock.getBoundingClientRect().top;
        let Ra = a2.getBoundingClientRect();
        let  R = Math.round(Ra.top + b2.getBoundingClientRect().height - bottomBorder + 110);  // селектор блока, при достижении верхнего края которого нужно открепить прилипающий элемент;  Math.round() только для IE; если ноль заменить на число, то блок будет прилипать до того, как нижний край элемента дойдёт до футера
        let topMinusConst = Ra.top - P2;

        if (topMinusConst <= 0) {

            if (topMinusConst <= R) {
                b2.className = 'block-stop';
                b2.style.top = -R + 'px';
            } else {
                b2.className = 'block-fix';
                b2.style.top = P2 + 'px';
            }
        } else {
            b2.className = '';
            b2.style.top = '';
        }
        window.addEventListener('resize', function() {
            a2.children[0].style.width = getComputedStyle(a2, '').width
        }, false);
    }
});
