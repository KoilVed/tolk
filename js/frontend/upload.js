$(document).ready(function() {
    let arrImages = [];

    function showPreview(objFileInput) {
        let $this = $(objFileInput)
        let form = $this.closest('.b-form');
        let imagesWrap = form.find('.b-form__images');
        let arr = objFileInput.files;
        Object.keys(arr).forEach(function(i) {
            let file = objFileInput.files[i];
            let fileReader = new FileReader();

            fileReader.onload = function(e) {
                imagesWrap.append('<div class="b-form__images-item"><img src="' + e.target.result + '" /></div>');
            }
            fileReader.readAsDataURL(objFileInput.files[i]);
        })
    }

    $('.js-load-img').on('change', function() {
        showPreview(this)
        arrImages.push($('.js-load-img').get(0).files);

    });

    $(document).on('click', '.b-form__images-item', function() {
        let $this = $(this);
        let number = $this.parent().find('.b-form__images-item').length - 1;
        $this.remove();

        let filesArr = $('.b-form__load input').get(0).files;

        var files = [].slice.call(filesArr);

        files.splice(number, 1);

        console.log(files);

    });

    $('.js-uploadForm').on('submit', function(e) {
        e.preventDefault();


    });
});
