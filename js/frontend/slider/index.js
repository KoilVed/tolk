import slick from 'slick-carousel';

let fullPageSlider = $('.js-fullpage-slider');
let newsSlider = $('.js-news-slider');
let newsMobileSlider = $('.js-news-mobile-slider');
let sectionSlider = $('.js-section-slider');
let sectionMobileSlider = $('.js-section-slider-mobile');
let sectionMiniSlider = $('.js-section-slider--mini');
let typographySlider = $('.js-slider-typography');


function getParamFullPageSlider(number) {
    return {
        dots        : true,
        customPaging: function(slider, i) {
            let number = i + 1;
            return `<span> 0${number} </span>`;
        },
        infinite    : false,
        speed       : 450,
        slidesToShow: 1,
        fade        : true,
        cssEase     : 'ease',
        appendArrows: $('.b-fullpage-slider__arrow'),
        prevArrow   : $(`.b-main-fullpage--${number} .js-arrow-prev`),
        nextArrow   : $(`.b-main-fullpage--${number} .js-arrow-next`)
    }
}

let paramNewsSlider = {
    dots        : false,
    infinite    : true,
    speed       : 450,
    slidesToShow: 1,
    fade        : true,
    cssEase     : 'ease'
};

let paramSectionSlider = {
    dots        : false,
    infinite    : true,
    speed       : 450,
    slidesToShow: 3,
    appendArrows: $('.section__arrow'),
    prevArrow   : $('.section__arrow .js-arrow-prev'),
    nextArrow   : $('.section__arrow .js-arrow-next'),
    responsive  : [
        {
            breakpoint: 1025,
            settings  : {
                variableWidth: true,
                swipeToSlide: true
            }
        },
        {
            breakpoint: 540,
            settings  : {
                slidesToShow: 1,
            }
        }
    ]
}

let paramSectionMobileSlider = {
    dots        : false,
    infinite    : true,
    speed       : 450,
    slidesToShow: 1,
    appendArrows: $('.section__arrow--mobile'),
    prevArrow   : $('.section__arrow--moblie .js-arrow-prev'),
    nextArrow   : $('.section__arrow--mobile .js-arrow-next'),
}

let paramSectionMiniSlider = {
    dots        : false,
    infinite    : true,
    speed       : 450,
    slidesToShow: 6,
    appendArrows: $('.section__arrow'),
    prevArrow   : $('.section__arrow--mini .js-arrow-prev'),
    nextArrow   : $('.section__arrow--mini .js-arrow-next'),
    variableWidth: true,
    swipeToSlide: true,
    responsive  : [
        {
            breakpoint: 1025,
            settings  : {
                slidesToShow  : 4,
                slidesToScroll: 1,
            }
        },
        {
            breakpoint: 712,
            settings  : {
                slidesToShow  : 4,
                slidesToScroll: 1,
                centerMode: true
            }
        },
    ]
};

let paramTypographySlider = {
    dots          : true,
    infinite      : false,
    slidesToShow  : 1,
    slidesToScroll: 1,
    speed         : 300,
    arrows        : true
};

let paramNewsMobileSlider = {
    dots          : true,
    infinite      : false,
    slidesToShow  : 1,
    slidesToScroll: 1,
    speed         : 300,
    arrows        : false
};

$(document).ready(function() {
    for (let i = 0; i < fullPageSlider.length; i++) {
        let config = getParamFullPageSlider(i);
        $(fullPageSlider[i]).slick(config);
    }

    newsSlider.slick(paramNewsSlider);

    sectionSlider.slick(paramSectionSlider);
    sectionMobileSlider.slick(paramSectionMobileSlider);
    sectionMiniSlider.slick(paramSectionMiniSlider);
    typographySlider.slick(paramTypographySlider);

});
